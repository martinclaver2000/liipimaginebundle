<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ThumbController extends AbstractController
{
    #[Route('/thumb', name: 'app_thumb')]
    public function index(): Response
    {
        return $this->render('thumb/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
